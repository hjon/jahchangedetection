//
//  ChangeDetection_Tests.m
//  ChangeDetection Tests
//
//  Created by Jon Hjelle on 9/7/13.
//  Copyright (c) 2013 Jon Hjelle. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "JAHChangeDetector.h"

@interface ChangeDetection_Tests : XCTestCase

@end

@implementation ChangeDetection_Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - Deletion detection tests

- (void)testNoDeleteDetection {
    NSArray* correctChanges = @[];

    NSArray* oldArray = @[];
    NSArray* newArray = @[@"1"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector deletionsForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[oldArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testSingleDeleteDetection {
    JAHItemChange* change = [[JAHItemChange alloc] init];
    change.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    NSArray* correctChanges = @[change];

    NSArray* oldArray = @[@"1"];
    NSArray* newArray = @[];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector deletionsForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testMultipleDeleteDetection {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeDelete;
    NSUInteger twoIndices[] = {0, 2};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSArray* oldArray = @[@"1", @"2", @"3"];
    NSArray* newArray = @[@"2"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector deletionsForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testNoDeleteDetectionWithKey {
    NSArray* correctChanges = @[];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"1", @"update": @"something"};

    NSArray* oldArray = @[object1];
    NSArray* newArray = @[object2];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector deletionsForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[oldArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testSingleDeleteDetectionWithKey {
    JAHItemChange* change = [[JAHItemChange alloc] init];
    change.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    NSArray* correctChanges = @[change];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};

    NSArray* oldArray = @[object1];
    NSArray* newArray = @[];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector deletionsForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testMultipleDeleteDetectionWithKey {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeDelete;
    NSUInteger twoIndices[] = {0, 2};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"nothing"};
    NSDictionary* object3 = @{@"id": @"3", @"update": @"nothing"};

    NSArray* oldArray = @[object1, object2, object3];
    NSArray* newArray = @[object2];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector deletionsForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

#pragma mark - Insertion detection tests

- (void)testNoInsertionDetection {
    NSArray* correctChanges = @[];

    NSArray* oldArray = @[@"1"];
    NSArray* newArray = @[];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector insertionsForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[oldArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testSingleInsertionDetection {
    JAHItemChange* change = [[JAHItemChange alloc] init];
    change.type = JAHItemChangeInsert;
    NSUInteger indices[] = {0, 0};
    change.newIndexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    NSArray* correctChanges = @[change];

    NSArray* oldArray = @[];
    NSArray* newArray = @[@"1"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector insertionsForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testMultipleInsertionDetection {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeInsert;
    NSUInteger indices[] = {0, 0};
    change1.newIndexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeInsert;
    NSUInteger twoIndices[] = {0, 2};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSArray* oldArray = @[@"2"];
    NSArray* newArray = @[@"1", @"2", @"3"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector insertionsForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testNoInsertionDetectionWithKey {
    NSArray* correctChanges = @[];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"1", @"update": @"something"};

    NSArray* oldArray = @[object1];
    NSArray* newArray = @[object2];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector insertionsForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[oldArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testSingleInsertionDetectionWithKey {
    JAHItemChange* change = [[JAHItemChange alloc] init];
    change.type = JAHItemChangeInsert;
    NSUInteger indices[] = {0, 0};
    change.newIndexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    NSArray* correctChanges = @[change];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};

    NSArray* oldArray = @[];
    NSArray* newArray = @[object1];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector insertionsForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testMultipleInsertionDetectionWithKey {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeInsert;
    NSUInteger indices[] = {0, 0};
    change1.newIndexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeInsert;
    NSUInteger twoIndices[] = {0, 2};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"nothing"};
    NSDictionary* object3 = @{@"id": @"3", @"update": @"nothing"};

    NSArray* oldArray = @[object2];
    NSArray* newArray = @[object1, object2, object3];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector insertionsForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

#pragma mark - Move detection tests

- (void)testNoMoveDetection {
    NSArray* correctChanges = @[];

    NSArray* oldArray = @[@"1", @"2"];
    NSArray* newArray = @[@"1", @"2"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector movesForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testSingleMoveDetection {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeMove;
    NSUInteger indices[] = {0, 1};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];
    NSUInteger newIndices[] = {0, 0};
    change1.newIndexPath = [NSIndexPath indexPathWithIndexes:newIndices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeMove;
    NSUInteger indices2[] = {0, 0};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:indices2 length:2];
    NSUInteger newIndices2[] = {0, 1};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:newIndices2 length:2];

    NSArray* correctChanges = @[change1, change2];

    NSArray* oldArray = @[@"1", @"3"];
    NSArray* newArray = @[@"3", @"1"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector movesForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testMultipleMoveDetection {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeMove;
    NSUInteger indices[] = {0, 2};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];
    NSUInteger newIndices[] = {0, 0};
    change1.newIndexPath = [NSIndexPath indexPathWithIndexes:newIndices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeMove;
    NSUInteger twoIndices[] = {0, 0};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];
    NSUInteger twoNewIndices[] = {0, 2};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:twoNewIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSArray* oldArray = @[@"1", @"2", @"3"];
    NSArray* newArray = @[@"3", @"2", @"1"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector movesForOldArray:@[oldArray] newArray:@[newArray] intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testNoMoveDetectionWithKey {
    NSArray* correctChanges = @[];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"something"};

    NSArray* oldArray = @[object1, object2];
    NSArray* newArray = @[object1, object2];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector movesForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[oldArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testSingleMoveDetectionWithKey {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeMove;
    NSUInteger indices[] = {0, 1};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];
    NSUInteger newIndices[] = {0, 0};
    change1.newIndexPath = [NSIndexPath indexPathWithIndexes:newIndices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeMove;
    NSUInteger indices2[] = {0, 0};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:indices2 length:2];
    NSUInteger newIndices2[] = {0, 1};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:newIndices2 length:2];

    NSArray* correctChanges = @[change1, change2];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"something"};

    NSArray* oldArray = @[object1, object2];
    NSArray* newArray = @[object2, object1];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector movesForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

- (void)testMultipleMoveDetectionWithKey {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeMove;
    NSUInteger indices[] = {0, 2};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];
    NSUInteger newIndices[] = {0, 0};
    change1.newIndexPath = [NSIndexPath indexPathWithIndexes:newIndices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeMove;
    NSUInteger twoIndices[] = {0, 0};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];
    NSUInteger twoNewIndices[] = {0, 2};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:twoNewIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"something"};
    NSDictionary* object3 = @{@"id": @"3", @"update": @"nothing"};

    NSArray* oldArray = @[object1, object2, object3];
    NSArray* newArray = @[object3, object2, object1];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* intermediateChanges = nil;
    NSArray* changes = [detector movesForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id" intermediateArray:&intermediateChanges];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
    XCTAssertEqualObjects(@[newArray], intermediateChanges, "Intermediate arrays are not equal.");
}

#pragma mark - Update detection tests

- (void)testNoUpdateDetection {
    NSArray* correctChanges = @[];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};

    NSArray* oldArray = @[object1];
    NSArray* newArray = @[object1];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* changes = [detector updatesForOldArray:@[oldArray] newArray:@[newArray]];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
}

- (void)testSingleUpdateDetection {
    JAHItemChange* change = [[JAHItemChange alloc] init];
    change.type = JAHItemChangeUpdate;
    NSUInteger indices[] = {0, 0};
    change.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    NSArray* correctChanges = @[change];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object1A = @{@"id": @"1", @"update": @"something"};

    NSArray* oldArray = @[object1];
    NSArray* newArray = @[object1A];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* changes = [detector updatesForOldArray:@[oldArray] newArray:@[newArray]];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
}

- (void)testMultipleUpdateDetection {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeUpdate;
    NSUInteger indices[] = {0, 0};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeUpdate;
    NSUInteger twoIndices[] = {0, 2};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object1A = @{@"id": @"1", @"update": @"something"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"nothing"};
    NSDictionary* object3 = @{@"id": @"3", @"update": @"nothing"};
    NSDictionary* object3A = @{@"id": @"3", @"update": @"something"};

    NSArray* oldArray = @[object1, object2, object3];
    NSArray* newArray = @[object1A, object2, object3A];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* changes = [detector updatesForOldArray:@[oldArray] newArray:@[newArray]];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
}

#pragma mark - Combined detection tests

- (void)testOneDeletionAndOneInsertion {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeInsert;
    NSUInteger twoIndices[] = {0, 0};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSArray* oldArray = @[@"1"];
    NSArray* newArray = @[@"2"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* changes = [[detector changesForOldArray:@[oldArray] newArray:@[newArray]] objectForKey:JAHRowChangesKey];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
}

- (void)testMultipleChanges {
    // 0, 1, 2, 3, 4, 5
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    // 1, 2, 3, 4, 5
    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeDelete;
    NSUInteger twoIndices[] = {0, 2};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    // 1, 3, 4, 5
    JAHItemChange* change3 = [[JAHItemChange alloc] init];
    change3.type = JAHItemChangeInsert;
    NSUInteger threeIndices[] = {0, 4};
    change3.newIndexPath = [NSIndexPath indexPathWithIndexes:threeIndices length:2];

    // 1, 3, 4, 5, 6
    JAHItemChange* change4 = [[JAHItemChange alloc] init];
    change4.type = JAHItemChangeMove;
    NSUInteger fourIndices[] = {0, 1};
    change4.indexPath = [NSIndexPath indexPathWithIndexes:fourIndices length:2];
    NSUInteger fourNewIndices[] = {0, 0};
    change4.newIndexPath = [NSIndexPath indexPathWithIndexes:fourNewIndices length:2];

    // 1, 3, 4, 5, 6
    JAHItemChange* change5 = [[JAHItemChange alloc] init];
    change5.type = JAHItemChangeMove;
    NSUInteger fiveIndices[] = {0, 5};
    change5.indexPath = [NSIndexPath indexPathWithIndexes:fiveIndices length:2];
    NSUInteger fiveNewIndices[] = {0, 1};
    change5.newIndexPath = [NSIndexPath indexPathWithIndexes:fiveNewIndices length:2];

    // 1, 5, 4, 3, 6
    JAHItemChange* change6 = [[JAHItemChange alloc] init];
    change6.type = JAHItemChangeMove;
    NSUInteger sixIndices[] = {0, 4};
    change6.indexPath = [NSIndexPath indexPathWithIndexes:sixIndices length:2];
    NSUInteger sixNewIndices[] = {0, 2};
    change6.newIndexPath = [NSIndexPath indexPathWithIndexes:sixNewIndices length:2];

    NSArray* correctChanges = @[change1, change2, change3, change4, change5, change6];

    // 0, 1, 2, 3, 4, 5 -> 1, 5, 4, 3, 6
    NSArray* oldArray = @[@"0", @"1", @"2", @"3", @"4", @"5"];
    NSArray* newArray = @[@"1", @"5", @"4", @"3", @"6"];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* changes = [[detector changesForOldArray:@[oldArray] newArray:@[newArray]] objectForKey:JAHRowChangesKey];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
}

- (void)testOneDeletionAndOneInsertionWithKey {
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeInsert;
    NSUInteger twoIndices[] = {0, 0};
    change2.newIndexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    NSArray* correctChanges = @[change1, change2];

    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"something"};

    NSArray* oldArray = @[object1];
    NSArray* newArray = @[object2];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* changes = [[detector changesForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id"] objectForKey:JAHRowChangesKey];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
}

- (void)testMultipleChangesWithKey {
    // 0, 1, 2, 3, 4, 5
    JAHItemChange* change1 = [[JAHItemChange alloc] init];
    change1.type = JAHItemChangeDelete;
    NSUInteger indices[] = {0, 0};
    change1.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

    // 1, 2, 3, 4, 5
    JAHItemChange* change2 = [[JAHItemChange alloc] init];
    change2.type = JAHItemChangeDelete;
    NSUInteger twoIndices[] = {0, 2};
    change2.indexPath = [NSIndexPath indexPathWithIndexes:twoIndices length:2];

    // 1, 3, 4, 5
    JAHItemChange* change3 = [[JAHItemChange alloc] init];
    change3.type = JAHItemChangeInsert;
    NSUInteger threeIndices[] = {0, 4};
    change3.newIndexPath = [NSIndexPath indexPathWithIndexes:threeIndices length:2];

    // 1, 3, 4, 5, 6
    JAHItemChange* change4 = [[JAHItemChange alloc] init];
    change4.type = JAHItemChangeMove;
    NSUInteger fourIndices[] = {0, 1};
    change4.indexPath = [NSIndexPath indexPathWithIndexes:fourIndices length:2];
    NSUInteger fourNewIndices[] = {0, 0};
    change4.newIndexPath = [NSIndexPath indexPathWithIndexes:fourNewIndices length:2];

    // 1, 3, 4, 5, 6
    JAHItemChange* change5 = [[JAHItemChange alloc] init];
    change5.type = JAHItemChangeMove;
    NSUInteger fiveIndices[] = {0, 5};
    change5.indexPath = [NSIndexPath indexPathWithIndexes:fiveIndices length:2];
    NSUInteger fiveNewIndices[] = {0, 1};
    change5.newIndexPath = [NSIndexPath indexPathWithIndexes:fiveNewIndices length:2];

    // 1, 5, 4, 3, 6
    JAHItemChange* change6 = [[JAHItemChange alloc] init];
    change6.type = JAHItemChangeMove;
    NSUInteger sixIndices[] = {0, 4};
    change6.indexPath = [NSIndexPath indexPathWithIndexes:sixIndices length:2];
    NSUInteger sixNewIndices[] = {0, 2};
    change6.newIndexPath = [NSIndexPath indexPathWithIndexes:sixNewIndices length:2];

    JAHItemChange* changeUpdate = [[JAHItemChange alloc] init];
    changeUpdate.type = JAHItemChangeUpdate;
    NSUInteger updateIdices[] = {0, 3};
    changeUpdate.indexPath = [NSIndexPath indexPathWithIndexes:updateIdices length:2];

    NSArray* correctChanges = @[change1, change2, change3, change4, change5, change6, changeUpdate];

    NSDictionary* object0 = @{@"id": @"0", @"update": @"nothing"};
    NSDictionary* object1 = @{@"id": @"1", @"update": @"nothing"};
    NSDictionary* object2 = @{@"id": @"2", @"update": @"something"};
    NSDictionary* object3 = @{@"id": @"3", @"update": @"nothing"};
    NSDictionary* object3A = @{@"id": @"3", @"update": @"something"};
    NSDictionary* object4 = @{@"id": @"4", @"update": @"nothing"};
    NSDictionary* object5 = @{@"id": @"5", @"update": @"something"};
    NSDictionary* object5A = @{@"id": @"5", @"update": @"something else"};
    NSDictionary* object6 = @{@"id": @"6", @"update": @"nothing"};

    // 0, 1, 2, 3, 4, 5 -> 1, 5, 4, 3, 6
    NSArray* oldArray = @[object0, object1, object2, object3, object4, object5];
    NSArray* newArray = @[object1, object5A, object4, object3A, object6];

    JAHChangeDetector* detector = [[JAHChangeDetector alloc] init];
    NSArray* changes = [[detector changesForOldArray:@[oldArray] newArray:@[newArray] uniqueKey:@"id"] objectForKey:JAHRowChangesKey];

    XCTAssertEqualObjects(correctChanges, changes, "Changes are not equal.");
}

@end
