//
//  JAHChangeDetector.h
//  ChangeDetection
//
//  Created by Jon Hjelle on 9/7/13.
//  Copyright (c) 2013 Jon Hjelle. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const JAHSectionChangesKey;
extern NSString* const JAHRowChangesKey;

@interface JAHChangeDetector : NSObject

@property (strong, nonatomic) NSArray* oldSections;
@property (strong, nonatomic) NSArray* currentSections;

- (NSArray*)deletionsForOldSections:(NSArray *)oldArray newSections:(NSArray *)newArray;
- (NSArray*)insertionsForOldSections:(NSArray *)oldArray newSections:(NSArray *)newArray;

- (NSArray*)deletionsForOldArray:(NSArray *)oldArray newArray:(NSArray *)newArray uniqueKey:(NSString*)uniqueKey intermediateArray:(NSArray**)arrayWithDeletionsApplied;
- (NSArray*)deletionsForOldArray:(NSArray *)oldArray newArray:(NSArray *)newArray intermediateArray:(NSArray**)arrayWithDeletionsApplied;
//- (NSArray*)deletionsForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray;
- (NSArray*)insertionsForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray uniqueKey:(NSString*)uniqueKey intermediateArray:(NSArray**)arrayWithInsertionsApplied;
- (NSArray*)insertionsForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray intermediateArray:(NSArray**)arrayWithInsertionsApplied;
//- (NSArray*)insertionsForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray;
- (NSArray*)movesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray uniqueKey:(NSString*)uniqueKey intermediateArray:(NSArray**)arrayWithMovesApplied;
- (NSArray*)movesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray intermediateArray:(NSArray**)arrayWithMovesApplied;
//- (NSArray*)movesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray;
- (NSArray*)updatesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray;
- (NSDictionary*)changesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray uniqueKey:(NSString*)uniqueKey;
- (NSDictionary*)changesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray;

@end


typedef NS_ENUM(NSUInteger, JAHItemChangeType) {
    JAHItemChangeInsert = 1,
    JAHItemChangeDelete = 2,
    JAHItemChangeMove = 3,
    JAHItemChangeUpdate = 4
};

@interface JAHItemChange : NSObject

@property (nonatomic) JAHItemChangeType type;
@property (nonatomic) NSUInteger sectionIndex;
@property (nonatomic, strong) NSIndexPath* indexPath;
@property (nonatomic, strong) NSIndexPath* newIndexPath;

@end
