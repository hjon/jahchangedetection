//
//  JAHChangeDetector.m
//  ChangeDetection
//
//  Created by Jon Hjelle on 9/7/13.
//  Copyright (c) 2013 Jon Hjelle. All rights reserved.
//

#import "JAHChangeDetector.h"

NSString* const JAHSectionChangesKey = @"JAHSectionChangesKey";
NSString* const JAHRowChangesKey = @"JAHRowChangesKey";

@implementation JAHChangeDetector

- (NSArray*)deletionsForOldSections:(NSArray *)oldArray newSections:(NSArray *)newArray {
    NSMutableArray* deletedItems = [oldArray mutableCopy];
    [deletedItems removeObjectsInArray:newArray];

    NSMutableArray* changes = [NSMutableArray arrayWithCapacity:[deletedItems count]];
    for (id object in deletedItems) {
        NSUInteger index = [oldArray indexOfObject:object];

        JAHItemChange* change = [[JAHItemChange alloc] init];
        change.type = JAHItemChangeDelete;
        change.sectionIndex = index;

        [changes addObject:change];
    }

    return [changes copy];
}

- (NSArray*)insertionsForOldSections:(NSArray *)oldArray newSections:(NSArray *)newArray {
    NSMutableArray* insertedItems = [newArray mutableCopy];
    [insertedItems removeObjectsInArray:oldArray];

    NSMutableArray* changes = [NSMutableArray arrayWithCapacity:[insertedItems count]];
    for (id object in insertedItems) {
        NSUInteger index = [newArray indexOfObject:object];

        JAHItemChange* change = [[JAHItemChange alloc] init];
        change.type = JAHItemChangeInsert;
        change.sectionIndex = index;

        [changes addObject:change];
    }

    return [changes copy];
}

- (NSArray*)deletionsForOldArray:(NSArray *)oldArrays newArray:(NSArray *)newArrays uniqueKey:(NSString *)uniqueKey intermediateArray:(NSArray**)arrayWithDeletionsApplied {
    NSMutableArray* intermediateArrays = [NSMutableArray array];
    NSMutableArray* changes = [NSMutableArray array];

    NSUInteger sectionIndex = 0;
    for (NSArray* oldArray in oldArrays) {
        NSMutableArray* intermediateArray = [NSMutableArray array];

        NSUInteger index = 0;
        for (id oldObject in oldArray) {
            id oldUniqueProperty;
            if (uniqueKey) {
                oldUniqueProperty = [oldObject valueForKey:uniqueKey];
            } else {
                oldUniqueProperty = oldObject;
            }

            id object;
            for (NSArray* newArray in newArrays) {
                for (id newObject in newArray) {
                    id newUniqueProperty;
                    if (uniqueKey) {
                        newUniqueProperty = [newObject valueForKey:uniqueKey];
                    } else {
                        newUniqueProperty = newObject;
                    }

                    if ([oldUniqueProperty isEqual:newUniqueProperty]) {
                        object = newObject;
                        break;
                    }
                }
                if (object) {
                    break;
                }
            }

            if (object) {
                [intermediateArray addObject:oldObject];
            } else {
                JAHItemChange* change = [[JAHItemChange alloc] init];
                change.type = JAHItemChangeDelete;
                NSUInteger indices[] = {sectionIndex, index};
                change.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];
                
                [changes addObject:change];
            }
            
            index++;
        }

        [intermediateArrays addObject:intermediateArray];
        sectionIndex++;
    }

    *arrayWithDeletionsApplied = [intermediateArrays copy];
    return [changes copy];
}

- (NSArray*)deletionsForOldArray:(NSArray *)oldArray newArray:(NSArray *)newArray intermediateArray:(NSArray**)arrayWithDeletionsApplied {
    return [self deletionsForOldArray:oldArray newArray:newArray uniqueKey:nil intermediateArray:arrayWithDeletionsApplied];
}

- (NSArray*)deletionsForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray {
    NSArray* intermediateArray = nil;
    return [self deletionsForOldArray:oldArray newArray:newArray intermediateArray:&intermediateArray];
}

- (NSArray*)insertionsForOldArray:(NSArray*)oldArrays newArray:(NSArray*)newArrays uniqueKey:(NSString*)uniqueKey intermediateArray:(NSArray**)arrayWithInsertionsApplied {
    NSMutableArray* intermediateArrays;
    if (*arrayWithInsertionsApplied) {
        intermediateArrays = [*arrayWithInsertionsApplied mutableCopy];
    } else {
        intermediateArrays = [oldArrays mutableCopy];
    }

    NSMutableArray* changes = [NSMutableArray array];

    NSUInteger sectionIndex = 0;
    for (NSArray* newArray in newArrays) {
        NSMutableArray* intermediateArray;
        if (sectionIndex == [intermediateArrays count]) {
            intermediateArray = [NSMutableArray array];
        } else {
            intermediateArray = [[intermediateArrays objectAtIndex:sectionIndex] mutableCopy];
        }

        NSUInteger index = 0;
        for (id newObject in newArray) {
            id newUniqueProperty;
            if (uniqueKey) {
                newUniqueProperty = [newObject valueForKey:uniqueKey];
            } else {
                newUniqueProperty = newObject;
            }

            id object;
            for (NSArray* oldArray in oldArrays) {
                for (id oldObject in oldArray) {
                    id oldUniqueProperty;
                    if (uniqueKey) {
                        oldUniqueProperty = [oldObject valueForKey:uniqueKey];
                    } else {
                        oldUniqueProperty = oldObject;
                    }

                    if ([newUniqueProperty isEqual:oldUniqueProperty]) {
                        object = oldObject;
                        break;
                    }
                }
                if (object) {
                    break;
                }
            }

            if (!object) {
                JAHItemChange* change = [[JAHItemChange alloc] init];
                change.type = JAHItemChangeInsert;
                NSUInteger indices[] = {sectionIndex, index};
                change.newIndexPath = [NSIndexPath indexPathWithIndexes:indices length:2];

                [intermediateArray insertObject:newObject atIndex:index];
                [changes addObject:change];
            }
            
            index++;
        }

        if (sectionIndex == [intermediateArrays count]) {
            [intermediateArrays addObject:intermediateArray];
        } else {
            [intermediateArrays replaceObjectAtIndex:sectionIndex withObject:intermediateArray];
        }
        sectionIndex++;
    }

    *arrayWithInsertionsApplied = [intermediateArrays copy];
    return [changes copy];
}

- (NSArray*)insertionsForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray intermediateArray:(NSArray**)arrayWithInsertionsApplied {
    return [self insertionsForOldArray:oldArray newArray:newArray uniqueKey:nil intermediateArray:arrayWithInsertionsApplied];
}

- (NSArray*)insertionsForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray {
    NSArray* intermediateArray = nil;
    return [self insertionsForOldArray:oldArray newArray:newArray intermediateArray:&intermediateArray];
}

- (NSArray*)movesForOldArray:(NSArray*)oldArrays newArray:(NSArray*)newArrays uniqueKey:(NSString*)uniqueKey intermediateArray:(NSArray**)arrayWithMovesApplied {
    NSMutableArray* intermediateArrays;
    if (*arrayWithMovesApplied) {
        intermediateArrays = [*arrayWithMovesApplied mutableCopy];
    } else {
        intermediateArrays = [NSMutableArray arrayWithCapacity:[oldArrays count]];
        for (NSArray* array in oldArrays) {
            [intermediateArrays addObject:[array mutableCopy]];
        }
    }

    NSMutableArray* changes = [NSMutableArray array];

    NSUInteger newSectionIndex = 0;
    for (NSArray* newArray in newArrays) {
        NSUInteger index = 0;
        for (id newObject in newArray) {
            id newUniqueProperty;
            if (uniqueKey) {
                newUniqueProperty = [newObject valueForKey:uniqueKey];
            } else {
                newUniqueProperty = newObject;
            }

            id object;
            NSUInteger oldSectionIndex = 0;
            NSUInteger oldIndex = 0;
            for (NSArray* oldArray in oldArrays) {
                oldIndex = 0;
                for (id oldObject in oldArray) {
                    id oldUniqueProperty;
                    if (uniqueKey) {
                        oldUniqueProperty = [oldObject valueForKey:uniqueKey];
                    } else {
                        oldUniqueProperty = oldObject;
                    }

                    if ([newUniqueProperty isEqual:oldUniqueProperty]) {
                        object = oldObject;
                        break;
                    }
                    
                    oldIndex++;
                }
                if (object) {
                    break;
                }

                oldSectionIndex++;
            }

            if (((newSectionIndex != oldSectionIndex) || (index != oldIndex)) && object) {
                // Can't use -removeObjectAtIndex: because after the first change, the other objects' indexes will have changed
                [intermediateArrays makeObjectsPerformSelector:@selector(removeObject:) withObject:object];
                [[intermediateArrays objectAtIndex:newSectionIndex] insertObject:object atIndex:index];

                JAHItemChange* change = [[JAHItemChange alloc] init];
                change.type = JAHItemChangeMove;
                NSUInteger oldIndices[] = {oldSectionIndex, oldIndex};
                change.indexPath = [NSIndexPath indexPathWithIndexes:oldIndices length:2];
                NSUInteger indices[] = {newSectionIndex, index};
                change.newIndexPath = [NSIndexPath indexPathWithIndexes:indices length:2];
                [changes addObject:change];
            }
            
            index++;
        }

        newSectionIndex++;
    }

    *arrayWithMovesApplied = [intermediateArrays copy];
    return [changes copy];
}

- (NSArray*)movesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray intermediateArray:(NSArray**)arrayWithMovesApplied {
    return [self movesForOldArray:oldArray newArray:newArray uniqueKey:nil intermediateArray:arrayWithMovesApplied];
}

- (NSArray*)movesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray {
    NSArray* intermediateArray = nil;
    return [self movesForOldArray:oldArray newArray:newArray intermediateArray:&intermediateArray];
}

- (NSArray*)updatesForOldArray:(NSArray*)oldArrays newArray:(NSArray*)newArrays {
    NSMutableArray* changes = [NSMutableArray array];

    NSUInteger sectionIndex = 0;
    for (NSArray* newArray in newArrays) {
        NSArray* oldArray = [oldArrays objectAtIndex:sectionIndex];

        NSUInteger oldCount = [oldArray count];
        NSUInteger index = 0;
        for (id newObject in newArray) {
            if ((index + 1) > oldCount) {
                break;
            }

            id oldObject = [oldArray objectAtIndex:index];
            if (![newObject isEqual:oldObject]) {
                JAHItemChange* change = [[JAHItemChange alloc] init];
                change.type = JAHItemChangeUpdate;
                NSUInteger indices[] = {sectionIndex, index};
                change.indexPath = [NSIndexPath indexPathWithIndexes:indices length:2];
                [changes addObject:change];
            }
            
            index++;
        }

        sectionIndex++;
    }

    return changes;
}

- (NSDictionary*)changesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray uniqueKey:(NSString*)uniqueKey {
    NSMutableArray* sectionChanges = [NSMutableArray array];
    if (self.oldSections || self.currentSections) {
        NSArray* sectionDeletions = [self deletionsForOldSections:self.oldSections newSections:self.currentSections];
        [sectionChanges addObjectsFromArray:sectionDeletions];

        NSArray* sectionInsertions = [self insertionsForOldSections:self.oldSections newSections:self.currentSections];
        [sectionChanges addObjectsFromArray:sectionInsertions];
    }

    NSMutableArray* rowChanges = [NSMutableArray array];
    NSArray* intermediateChanges = nil;

    NSArray* deletions = [self deletionsForOldArray:oldArray newArray:newArray uniqueKey:uniqueKey intermediateArray:&intermediateChanges];

    BOOL validDelete = NO;
    NSMutableArray* actualDeletes = [NSMutableArray arrayWithCapacity:[deletions count]];
    for (JAHItemChange* delete in deletions) {
        validDelete = YES;
        for (JAHItemChange* sectionChange in sectionChanges) {
            if ([delete.indexPath indexAtPosition:0] == sectionChange.sectionIndex) {
                validDelete = NO;
                break;
            }
        }

        if (validDelete) {
            [actualDeletes addObject:delete];
        }
    }

    [rowChanges addObjectsFromArray:actualDeletes];

    NSArray* insertions = [self insertionsForOldArray:oldArray newArray:newArray uniqueKey:uniqueKey intermediateArray:&intermediateChanges];

    BOOL validInsert = NO;
    NSMutableArray* actualInserts = [NSMutableArray arrayWithCapacity:[insertions count]];
    for (JAHItemChange* insert in insertions) {
        validInsert = YES;
        for (JAHItemChange* sectionChange in sectionChanges) {
            if ([insert.newIndexPath indexAtPosition:0] == sectionChange.sectionIndex) {
                validInsert = NO;
                break;
            }
        }

        if (validInsert) {
            [actualInserts addObject:insert];
        }
    }

    [rowChanges addObjectsFromArray:actualInserts];

    NSArray* moves = [self movesForOldArray:oldArray newArray:newArray uniqueKey:uniqueKey intermediateArray:&intermediateChanges];
    [rowChanges addObjectsFromArray:moves];

    NSArray* updates = [self updatesForOldArray:intermediateChanges newArray:newArray];

    BOOL validUpdate = NO;
    NSMutableArray* actualUpdates = [NSMutableArray arrayWithCapacity:[updates count]];
    for (JAHItemChange* update in updates) {
        validUpdate = YES;
        for (JAHItemChange* change in rowChanges) {
            if ([update.indexPath isEqual:change.newIndexPath]) {
                validUpdate = NO;
                break;
            }
        }

        if (validUpdate) {
            [actualUpdates addObject:update];
        }
    }

    [rowChanges addObjectsFromArray:actualUpdates];

    NSDictionary* changes = @{JAHSectionChangesKey: [sectionChanges copy], JAHRowChangesKey: [rowChanges copy]};
    return changes;
}

- (NSDictionary*)changesForOldArray:(NSArray*)oldArray newArray:(NSArray*)newArray {
    return [self changesForOldArray:oldArray newArray:newArray uniqueKey:nil];
}

@end


@implementation JAHItemChange

- (NSString*)description {
    NSString* type;
    switch (self.type) {
        case JAHItemChangeDelete:
            type = @"Deletion";
            break;
        case JAHItemChangeInsert:
            type = @"Insertion";
            break;
        case JAHItemChangeMove:
            type = @"Moved";
            break;
        case JAHItemChangeUpdate:
            type = @"Updated";
            break;
    }

    return [NSString stringWithFormat:@"%@ with indexPath %@ and newIndexPath %@", type, self.indexPath, self.newIndexPath];
}

- (NSIndexPath*)newIndexPath {
    return [_newIndexPath copy];
}

- (BOOL)isEqualToJAHItemChange:(JAHItemChange*)itemChange {
    if (!itemChange) {
        return NO;
    }

    BOOL haveEqualTypes = (!self.type && !itemChange.type) || (self.type == itemChange.type);
    BOOL haveEqualIndexPaths = (!self.indexPath && !itemChange.indexPath) || [self.indexPath isEqual:itemChange.indexPath];
    BOOL haveEqualNewIndexPaths = (!self.newIndexPath && !itemChange.newIndexPath) || [self.newIndexPath isEqual:itemChange.newIndexPath];

    return haveEqualTypes && haveEqualIndexPaths && haveEqualNewIndexPaths;
}

#pragma mark - NSObject

- (BOOL)isEqual:(id)object {
    if (self == object) {
        return YES;
    }

    if (![object isKindOfClass:[JAHItemChange class]]) {
        return NO;
    }

    return [self isEqualToJAHItemChange:(JAHItemChange*)object];
}

- (NSUInteger)hash {
    return [self.indexPath hash] ^ [self.newIndexPath hash] * self.type;
}

@end
