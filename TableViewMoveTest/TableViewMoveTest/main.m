//
//  main.m
//  TableViewMoveTest
//
//  Created by Jon Hjelle on 9/12/13.
//  Copyright (c) 2013 Jon Hjelle. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JAHAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JAHAppDelegate class]));
    }
}
