//
//  JAHMasterViewController.m
//  TableViewMoveTest
//
//  Created by Jon Hjelle on 9/12/13.
//  Copyright (c) 2013 Jon Hjelle. All rights reserved.
//

#import "JAHMasterViewController.h"

@interface JAHMasterViewController ()
@property (strong) NSMutableArray* objects;
@end

@implementation JAHMasterViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;

    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(modify:)];
    self.navigationItem.rightBarButtonItem = addButton;

//    self.objects = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", @"4", @"5", nil]; // Test 1
//    self.objects = [NSMutableArray arrayWithObjects:@"0", @"1", @"2", @"3", @"4", nil]; // Test 2
//    self.objects = [NSMutableArray arrayWithObjects:@"1", @"3", nil]; // Test 3
//    self.objects = [NSMutableArray arrayWithObjects:@"0", @"1", @"2", @"3", @"4", @"5", nil]; // Test 4
//    self.objects = [NSMutableArray arrayWithObjects:@"1", @"3", nil]; // Test 5
//    self.objects = [NSMutableArray arrayWithObjects:@"1", @"2", @"3", nil]; // Test 6
    NSMutableArray* array1 = [NSMutableArray arrayWithObjects:@"0", @"1", nil];
    NSMutableArray* array2 = [NSMutableArray arrayWithObjects:@"2", @"3", @"4", @"5", nil];
    self.objects = [NSMutableArray arrayWithObjects:array1, array2, nil]; // Test 7
}

- (void)modify:(id)sender {
    [self.tableView beginUpdates];

// 1, 2, 3, 4, 5 -> 4, 3, 2, 1, 0 // Test 1
//    [self.tableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
//    [self.tableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
//    [self.tableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
//    [self.tableView moveRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0] toIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];

// 0, 1, 2, 3, 4 -> 1, 2 // Test 2
//    [self.objects removeObjectAtIndex:0];
//    [self.objects removeObjectAtIndex:2];
//    [self.objects removeObjectAtIndex:2];
//
//    NSIndexPath* one = [NSIndexPath indexPathForRow:0 inSection:0];
//    NSIndexPath* two = [NSIndexPath indexPathForRow:3 inSection:0];
//    NSIndexPath* three = [NSIndexPath indexPathForRow:4 inSection:0];
//    [self.tableView deleteRowsAtIndexPaths:@[one, two, three] withRowAnimation:UITableViewRowAnimationAutomatic];

// 1, 3 -> 0, 1, 2, 3, 4 // Test 3
//    [self.objects insertObject:@"0" atIndex:0];
//    [self.objects insertObject:@"2" atIndex:2];
//    [self.objects insertObject:@"4" atIndex:4];
//
//    NSIndexPath* one = [NSIndexPath indexPathForRow:0 inSection:0];
//    NSIndexPath* two = [NSIndexPath indexPathForRow:2 inSection:0];
//    NSIndexPath* three = [NSIndexPath indexPathForRow:4 inSection:0];
//    [self.tableView insertRowsAtIndexPaths:@[one, two, three] withRowAnimation:UITableViewRowAnimationAutomatic];

// 0, 1, 2, 3, 4, 5 -> 1, 5, 4, 3, 6 // Test 4
// 1, 5, 4, 3, 6
//    [self.objects removeObjectAtIndex:0]; // 1, 2, 3, 4, 5
//    [self.objects removeObjectAtIndex:1]; // 1, 3, 4, 5
//    [self.objects insertObject:@"6" atIndex:4]; // 1, 3, 4, 5, 6
//    [self.objects exchangeObjectAtIndex:1 withObjectAtIndex:3]; // 1, 5, 4, 3, 6
//
//    NSIndexPath* zero = [NSIndexPath indexPathForRow:0 inSection:0]; // Remove 0 -> 1, 2, 3, 4, 5
//    NSIndexPath* two = [NSIndexPath indexPathForRow:2 inSection:0]; // Remove 2 -> 1, 3, 4, 5
//    [self.tableView deleteRowsAtIndexPaths:@[zero, two] withRowAnimation:UITableViewRowAnimationAutomatic];
//    NSIndexPath* six = [NSIndexPath indexPathForRow:4 inSection:0]; // Add 6 -> 1, 3, 4, 5, 6
//    [self.tableView insertRowsAtIndexPaths:@[six] withRowAnimation:UITableViewRowAnimationAutomatic];
//    NSIndexPath* moveFive = [NSIndexPath indexPathForRow:5 inSection:0];
//    NSIndexPath* moveFiveNew = [NSIndexPath indexPathForRow:1 inSection:0];
//    [self.tableView moveRowAtIndexPath:moveFive toIndexPath:moveFiveNew]; // Move 5 -> 1, 5, 3, 4, 6
//    //NSIndexPath* moveThree = [NSIndexPath indexPathForRow:3 inSection:0]; // It looks like even for moves, use the original positions
//    //NSIndexPath* moveThreeNew = [NSIndexPath indexPathForRow:3 inSection:0];
//    //[self.tableView moveRowAtIndexPath:moveThree toIndexPath:moveThreeNew]; // Move 3 -> 1, 5, 4, 3, 6
//    NSIndexPath* moveFour = [NSIndexPath indexPathForRow:4 inSection:0]; // "Duplicate moves" seem to also be ok (this move should have been covered by moving the '3', but moving the '4' also works and is easier to detect (and it looks like I can move the '3' or the '4' or both)
//    NSIndexPath* moveFourNew = [NSIndexPath indexPathForRow:2 inSection:0];
//    [self.tableView moveRowAtIndexPath:moveFour toIndexPath:moveFourNew]; // Move 4 -> 1, 5, 4, 3, 6
//    NSIndexPath* moveOne = [NSIndexPath indexPathForRow:1 inSection:0]; // Again, 'duplicate moves' seems to be ok (this move is just fine even though the 1 has already been 'moved' via the removal of the '0'.
//    NSIndexPath* moveOneNew = [NSIndexPath indexPathForRow:0 inSection:0];
//    [self.tableView moveRowAtIndexPath:moveOne toIndexPath:moveOneNew]; // Move 1 -> 1, 5, 4, 3, 6

// 1, 3 -> 3, 1 // Test 5
//    [self.objects exchangeObjectAtIndex:0 withObjectAtIndex:1];
//    NSIndexPath* moveFirst = [NSIndexPath indexPathForRow:0 inSection:0];
//    NSIndexPath* moveSecond = [NSIndexPath indexPathForRow:1 inSection:0];
//    [self.tableView moveRowAtIndexPath:moveFirst toIndexPath:moveSecond];

// 1, 2, 3 -> 3, 2, 1 // Test 6
//    [self.objects exchangeObjectAtIndex:0 withObjectAtIndex:2];
//    NSIndexPath* moveFirst = [NSIndexPath indexPathForRow:0 inSection:0];
//    NSIndexPath* moveSecond = [NSIndexPath indexPathForRow:2 inSection:0];
//    [self.tableView moveRowAtIndexPath:moveFirst toIndexPath:moveSecond];
//    NSIndexPath* moveThird = [NSIndexPath indexPathForRow:2 inSection:0];
//    NSIndexPath* moveFourth = [NSIndexPath indexPathForRow:0 inSection:0];
//    [self.tableView moveRowAtIndexPath:moveThird toIndexPath:moveFourth];

// [0, 1], [2, 3, 4, 5] -> [1, 5], [4, 3, 6] // Test 7
// [1, 5], [4, 3, 6]
    [self.objects[0] removeObjectAtIndex:0]; // [1], [2, 3, 4, 5]
    [self.objects[1] removeObjectAtIndex:0]; // [1], [3, 4, 5]
    [self.objects[1] insertObject:@"6" atIndex:3]; // [1], [3, 4, 5, 6]
    [self.objects[1] removeObjectAtIndex:2]; // [1], [3, 4, 6]
    [self.objects[0] insertObject:@"5" atIndex:1]; // [1, 5], [3, 4, 6]
    [self.objects[1] exchangeObjectAtIndex:0 withObjectAtIndex:1]; // [1, 5], [4, 3, 6]

    NSIndexPath* zero = [NSIndexPath indexPathForRow:0 inSection:0]; // Remove 0 -> [1], [2, 3, 4, 5]
    NSIndexPath* two = [NSIndexPath indexPathForRow:0 inSection:1]; // Remove 2 -> [1], [3, 4, 5]
    [self.tableView deleteRowsAtIndexPaths:@[zero, two] withRowAnimation:UITableViewRowAnimationAutomatic];
    NSIndexPath* six = [NSIndexPath indexPathForRow:2 inSection:1]; // Add 6 -> [1], [3, 4, 5, 6]
    [self.tableView insertRowsAtIndexPaths:@[six] withRowAnimation:UITableViewRowAnimationAutomatic];
    NSIndexPath* moveFive = [NSIndexPath indexPathForRow:3 inSection:1];
    NSIndexPath* moveFiveNew = [NSIndexPath indexPathForRow:1 inSection:0];
    [self.tableView moveRowAtIndexPath:moveFive toIndexPath:moveFiveNew]; // Move 5 -> [1, 5], [3, 4, 6]
    NSIndexPath* moveFour = [NSIndexPath indexPathForRow:2 inSection:1]; // "Duplicate moves" seem to also be ok (this move should have been covered by moving the '3', but moving the '4' also works and is easier to detect (and it looks like I can move the '3' or the '4' or both)
    NSIndexPath* moveFourNew = [NSIndexPath indexPathForRow:0 inSection:1];
    [self.tableView moveRowAtIndexPath:moveFour toIndexPath:moveFourNew]; // Move 4 -> [1, 5], [4, 3, 6]
    NSIndexPath* moveOne = [NSIndexPath indexPathForRow:1 inSection:0]; // Again, 'duplicate moves' seems to be ok (this move is just fine even though the 1 has already been 'moved' via the removal of the '0'.
    NSIndexPath* moveOneNew = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView moveRowAtIndexPath:moveOne toIndexPath:moveOneNew]; // Move 1 -> [1, 5], [4, 3, 6]

    [self.tableView endUpdates];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)insertNewObject:(id)sender
{
    if (!_objects) {
        _objects = [[NSMutableArray alloc] init];
    }
    [_objects insertObject:[NSDate date] atIndex:0];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return 1;
    return 2; // Test 7
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return _objects.count;
    return [_objects[section] count]; // Test 7
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

//    id object = _objects[indexPath.row];
    id object = _objects[indexPath.section][indexPath.row]; // Test 7
    cell.textLabel.text = [object description];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [_objects removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

@end
