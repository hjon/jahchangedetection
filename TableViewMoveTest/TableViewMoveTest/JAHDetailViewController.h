//
//  JAHDetailViewController.h
//  TableViewMoveTest
//
//  Created by Jon Hjelle on 9/12/13.
//  Copyright (c) 2013 Jon Hjelle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JAHDetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;

@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@end
